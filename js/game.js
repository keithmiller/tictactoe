
var canvas = document.getElementById("canvas");
var context = canvas.getContext("2d");
var mouse = {}; //initializes mouse variable to store its current position
var boxesX = 3; // number or spaces in the x dimension
var boxesY = 3; // number or spaces in the y dimension
var boxWidth = 196; //width of each space
var xoSize = boxWidth/2 - 20; // size of X and O
var lineWidth = 6; //width of space between space
var width = boxesX*boxWidth+(boxesX-1)*lineWidth; //width of canvas
var height = boxesY*boxWidth+(boxesY-1)*lineWidth; //height of canvas
var userX; //becomes a boolean that determines which shape the user and machine play
var gameOver = false; //boolean that ends the game when true
var userWins = 0; //total user wins for current session
var machineWins = 0; //total machine wins for current session
var draws = 0; //total draws for current session

//accesses session storage to get wins/losses/draws, if not set, they are set to 0
if (!sessionStorage.wins) {
    sessionStorage.wins = 0;
} else {
	userWins = sessionStorage.wins;
}
if (!sessionStorage.losses) {
    sessionStorage.losses = 0;
} else {
	machineWins = sessionStorage.losses;
}
if (!sessionStorage.draws) {
    sessionStorage.draws = 0;
} else {
	draws = sessionStorage.draws;
}
//displays wins/losses/draws
document.getElementById("winLoss").innerHTML="Wins: "+userWins+" Losses: "+machineWins+" Draws: "+draws;

//randomly chooses who goes first and makes sure they use an X
function chooseStart(){
	if(Math.floor(Math.random() * 2) == 0){
		machinePlay();
		userX = false;
	} else {
		userX = true;
		alert("Your move");
	}

}

//captures location of click
canvas.addEventListener("click", function(event){
	var mouseX = event.pageX;
	var mouseY = event.pageY;
	var boardX;
	var boardY;
	boxClicked(mouseX,mouseY);
	console.log("clicked: " + mouseX+", "+mouseY)
});

//determines which box was clicked on and plays that space
function boxClicked(mouseX,mouseY){
		if(mouseX <= boxWidth && mouseY <= boxWidth) {
			userPlay(a1);
		} else if(mouseX <=boxWidth && mouseY >=(boxWidth+lineWidth) && mouseY <=(2*boxWidth+lineWidth)){
			userPlay(a2);
		} else if(mouseX <=boxWidth && mouseY >=(2*(boxWidth+lineWidth)) && mouseY <=width){
			userPlay(a3);
		} else if(mouseX >=(boxWidth+lineWidth) && mouseX <=(2*boxWidth+lineWidth) && mouseY <= boxWidth) {
			userPlay(b1);
		} else if(mouseX >=(boxWidth+lineWidth) && mouseX <=(2*boxWidth+lineWidth) && mouseY >=(boxWidth+lineWidth) && mouseY <=(2*boxWidth+lineWidth)){
			userPlay(b2);
		} else if(mouseX >=(boxWidth+lineWidth) && mouseX <=(2*boxWidth+lineWidth) && mouseY >=404 && mouseY <=width){
			userPlay(b3);
		} else if(mouseX >=(2*(boxWidth+lineWidth)) && mouseX <=width && mouseY <= boxWidth){
			userPlay(c1);
		} else if(mouseX >=(2*(boxWidth+lineWidth)) && mouseX >=(boxWidth+lineWidth) && mouseY <=(2*boxWidth+lineWidth)){
			userPlay(c2);
		} else if(mouseX >=(2*(boxWidth+lineWidth)) && mouseX >=(2*(boxWidth+lineWidth)) && mouseY <=width){
			userPlay(c3);
		} else {

		}
		
}

//scoring is set for a 3x3, each variable is a row, column, or diagonal. 
//If any of these reaches 3, the game ends
var userScore = {
	numA: 0,
	numB: 0,
	numC: 0,
	num1: 0,
	num2: 0,
	num3: 0,
	d1: 0,
	d2: 0
}
var machineScore = {
	numA: 0,
	numB: 0,
	numC: 0,
	num1: 0,
	num2: 0,
	num3: 0,
	d1: 0,
	d2: 0
}

//startX and startY are the top left corner of the box, which is the point from which X and O are drawn
//empty states whether the box is empty or not, and changes when it is taken
//playValue is which column, row and diagonal it is part of, is added to the total 
var a1 = {
	startX: 0,
	startY: 0,
	empty: true,
	playValue: [1,0,0,1,0,0,1,0]
	// [a,b,c,1,2,3,diagonal1,diagonal2]
}
var a2 = {
	startX: 0,
	startY: boxWidth+lineWidth,
	empty: true,
	playValue: [1,0,0,0,1,0,0,0]
}
var a3 = {
	startX: 0,
	startY: 2*(boxWidth+lineWidth),
	empty: true,
	playValue: [1,0,0,0,0,1,0,1]
}
var b1 = {
	startX: boxWidth+lineWidth,
	startY: 0,
	empty: true,
	playValue: [0,1,0,1,0,0,0,0]
}
var b2 = {
	startX: boxWidth+lineWidth,
	startY: boxWidth+lineWidth,
	empty: true,
	playValue: [0,1,0,0,1,0,1,1]
}
var b3 = {
	startX: boxWidth+lineWidth,
	startY: 2*(boxWidth+lineWidth),
	empty: true,
	playValue: [0,1,0,0,0,1,0,0]
}
var c1 = {
	startX: 2*(boxWidth+lineWidth),
	startY: 0,
	empty: true,
	playValue: [0,0,1,1,0,0,0,1]
}
var c2 = {
	startX: 2*(boxWidth+lineWidth),
	startY: boxWidth+lineWidth,
	empty: true,
	playValue: [0,0,1,0,1,0,0,0]
}
var c3 = {
	startX: 2*(boxWidth+lineWidth),
	startY: 2*(boxWidth+lineWidth),
	empty: true,
	playValue: [0,0,1,0,0,1,1,0]

}

//array of the spaces that haven't been used yet
var emptySpaces = [a1,a2,a3,b1,b2,b3,c1,c2,c3];


//draws the board depending on the variables set at thhe top of this document
function drawCanvas(){
	context.canvas.width  = width;
	context.canvas.height = height;
    context.fillStyle = "black";
    context.fillRect(0,0,width,height);
    context.fillStyle = "#fff";
    for (var i = 0; i < boxesY; i++) {
    	for (var j = 0; j < boxesX; j++) {
    		context.fillRect(j*(boxWidth+lineWidth),i*(boxWidth+lineWidth),boxWidth,boxWidth);
    	}
    }
}

//draws an X in the given box and calls setTaken
function drawX(box) {
	var startX = box.startX + boxWidth/2;
	var startY = box.startY + boxWidth/2;
	context.moveTo(startX - xoSize, startY - xoSize);
    context.lineTo(startX + xoSize, startY + xoSize);
    context.moveTo(startX + xoSize, startY - xoSize);
    context.lineTo(startX - xoSize, startY + xoSize);
    context.strokeStyle = '#444'; 
   	context.lineWidth = 5;
    context.stroke();
    setTaken(box);
}
function drawO(box){
	var startX = box.startX + boxWidth/2;
	var startY = box.startY + boxWidth/2;
	context.beginPath();
	context.arc(startX,startY,xoSize,0,2*Math.PI);
	context.strokeStyle = '#444'; 
   	context.lineWidth = 5;
	context.stroke();
	setTaken(box);
}

//sets the box state to taken, removes it from the array of empty spaces
function setTaken(box){
	box.empty = false;
	var taken = emptySpaces.indexOf(box);
	emptySpaces.splice(taken, 1);
}

//adds the play value of the clicked box to the totals of each player
//tracks for game end
function trackGame(box,player){
	if(player){
		userScore.numA += box.playValue[0];
		userScore.numB += box.playValue[1];
		userScore.numC += box.playValue[2];
		userScore.num1 += box.playValue[3];
		userScore.num2 += box.playValue[4];
		userScore.num3 += box.playValue[5];
		userScore.d1 += box.playValue[6];
		userScore.d2 += box.playValue[7];

		//checks if any of the values in the user score equals three
		//if so, user wins. Win added to total
		for(var prop in userScore) {
		    if(userScore.hasOwnProperty(prop)) {
		        if(userScore[prop] == 3) {
		            alert("Game end. You win!");
		            sessionStorage.wins = Number(sessionStorage.wins) + 1;
		            gameOver = true;
		        } 
		    }
		}
	} else {
		machineScore.numA += box.playValue[0];
		machineScore.numB += box.playValue[1];
		machineScore.numC += box.playValue[2];
		machineScore.num1 += box.playValue[3];
		machineScore.num2 += box.playValue[4];
		machineScore.num3 += box.playValue[5];
		machineScore.d1 += box.playValue[6];
		machineScore.d2 += box.playValue[7];

		//checks if any of the values in the machine score equals three
		//if so, user loses. Loss added to total 
		for(var prop in machineScore) {
		    if(machineScore.hasOwnProperty(prop)) {
		        if(machineScore[prop] == 3) {
		            alert("Game end. You lose!");
		            sessionStorage.losses = Number(sessionStorage.losses) + 1;
		            gameOver = true;
		        }
		    }
		}
	}
	
	//if all the empty spaces are taken up and there is no winner, the game is a draw
	if(emptySpaces.length == 0 && !gameOver){
		alert("It's a draw!");
		sessionStorage.draws = Number(sessionStorage.draws) + 1;
		gameOver = true;
	}
}

//makes sure that the user clicks on an empty space
//if so, draws the appropriate symbol in that box, tracks score through trackGame function
//if game continues, machine plays after 1 second
function userPlay(box){
	if(box.empty){
		if(userX){
			drawX(box)
		} else {
			drawO(box);
		}
		trackGame(box,true);
		if (!gameOver) {
			setTimeout(machinePlay(), 1000);
		} else {
			gameEnd();
		}
			
	} else {
		alert("This spot is already taken");
	}
}

//machine picks a random space within the array of empty spaces
//Draws an X or O in that box
//takes half a second before tracking game and seeing if the game is over
function machinePlay(){
	var randSpace = Math.floor(Math.random() * (emptySpaces.length));
	var playEmpty = emptySpaces[randSpace]; 
		if (userX) {
		 	drawO(playEmpty);
		} else{
		 	drawX(playEmpty);
		}
		setTimeout(function(){
			trackGame(playEmpty,false);
			if (gameOver) {
				gameEnd();
			} 
		},500);
}

function gameEnd(){
	window.location.reload();
}

//initiates game
drawCanvas();
chooseStart();














